package routers

import (
	"encoding/json"
	"gitlab.com/kevynestrada/test-api/helper"
	"net/http"
)
func Rotate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, POST")
	var squareMatrix [][]int
	err := json.NewDecoder(r.Body).Decode(&squareMatrix)
	if err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte(err.Error()))
		return
	}
	err = helper.ValidArrayIsNxN(squareMatrix)
	if err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(helper.RotateArrayMinus90(squareMatrix))
}