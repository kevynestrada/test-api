package helper

import (
	"errors"
	"log"
)

const (
	LEFT = 1
	RIGHT = 2
)

func ValidArrayIsNxN(array [][]int) error {
	var rows int
	rows = len(array)
	for _, subarray := range array {
		if len(subarray) != rows {
			return errors.New("Los datos de entrada no estan correctamente formateados (N x N)")
		}
	}
	return nil
}

func makeArrayNxN(size int) [][]int {
	array := make([][]int, size)
	for i := 0; i < size; i++ {
		array[i] = make([]int, size)
	}
	return array
}

func RotateArrayMinus90(arrayIn [][]int) ([][]int) {
	size := len(arrayIn)
	arrayOut := makeArrayNxN(size)
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			log.Println(arrayIn[j][size - i - 1])
			arrayOut[i][j] = arrayIn[j][size - i - 1]
		}
	}
	return arrayOut
}