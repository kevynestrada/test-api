package main

import (
	"gitlab.com/kevynestrada/test-api/routers"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/v1/rotate", routers.Rotate).Methods("POST")
	log.Fatal(http.ListenAndServe(":8080", r))
}
